  
  Licensed Materials - Property of IBM
  
  IBM Power Vector Intrinisic Functions version 1.0.6
  
  Copyright IBM Corp. 2015,2017
  US Government Users Restricted Rights - Use, duplication or
  disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
  
  See the licence in the license subdirectory.
  
  More information on this software is available on the IBM DeveloperWorks
  website at
  https://www.ibm.com/developerworks/community/groups/community/powerveclib
  

vec64int.h:

  Set:

    vec_set8sb:
      __m64 vec_set8sb (char c7, char c6, char c5, char c4, char c3, char c2, char c1, char c0)
        Set 8 8-bit chars

    vec_set4sh:
      __m64 vec_set4sh (short s3, short s2, short s1, short s0)
        Set 4 16-bit shorts

    vec_set2sw:
      __m64 vec_set2sw (int i1, int i0)
        Set 4 32-bit ints

  Store:

    vec_store1dstream:
      void vec_store1dstream (__m64* to, __m64 from)
        Store long long using a non-temporal memory hint

    vec_storebyvectormask8b:
      void vec_storebyvectormask8b (__m64 from, __m64 mask, char* to)
        Store 8 8-bit chars under mask

  Insert:

    vec_insert1hinto4h:
      __m64 vec_insert1hinto4h (__m64 into, int from, intlit2 element_number)
        Insert 16-bit short into one of 4 16-bit shorts

  Extract:

    vec_extract1h3zfrom4h:
      int vec_extract1h3zfrom4h (__m64 from, intlit2 element_number)
        Extract 16-bit short from one of 4 16-bit shorts, zeroing upper

    vec_extractupperbit8b:
      int vec_extractupperbit8b (__m64 from)
        Extract upper bit of 8 chars

  Convert Floating-Point to Integer:

    vec_convert4sptolower4of8sb:
      __m64 vec_convert4sptolower4of8sb (__m128 a)
        Convert 4 32-bit floats to 8-bit chars and insert

    vec_convert4spto4sh:
      __m64 vec_convert4spto4sh (__m128 a)
        Convert 4 32-bit floats to 16-bit shorts

    vec_convertlower2of4spto2sw:
      __m64 vec_convertlower2of4spto2sw (__m128 a)
        Convert lower 2 32-bit floats to 32-bit ints

    vec_convertlower2of4spto2swtruncated:
      __m64 vec_convertlower2of4spto2swtruncated (__m128 a)
        Convert lower 2 32-bit floats to 32-bit ints with truncation

    vec_convertlower1of4sptosw:
      int vec_convertlower1of4sptosw (__m128 a)
        Convert lower 32-bit float to 32-bit int

    vec_convertlower1of4sptoswtruncated:
      int vec_convertlower1of4sptoswtruncated (__m128 a)
        Convert lower 32-bit float to 32-bit int with truncation

    vec_convertlower1of4spto1sd:
      long long vec_convertlower1of4spto1sd (__m128 a)
        Convert lower 32-bit float to 64-bit long long

  Boolean:

    vec_bitwiseand1d:
      __m64 vec_bitwiseand1d (__m64 left, __m64 right)
        Bitwise 64-bit and

    vec_bitwisexor1d:
      __m64 vec_bitwisexor1d (__m64 left, __m64 right)
        Bitwise 64-bit xor

  Arithmetic:

    vec_multiply4uhupper:
      __m64 vec_multiply4uhupper (__m64 left, __m64 right)
        Multiply 4 unsigned 16-bit shorts producing upper halves

    vec_average8ub:
      __m64 vec_average8ub (__m64 left, __m64 right)
        Average 8 8-bit unsigned chars

    vec_average4uh:
      __m64 vec_average4uh (__m64 left, __m64 right)
        Average 4 16-bit unsigned shorts

    vec_max8ub:
      __m64 vec_max8ub (__m64 left, __m64 right)
        Max 8 8-bit unsigned chars

    vec_max4sh:
      __m64 vec_max4sh (__m64 left, __m64 right)
        Max 4 16-bit shorts

    vec_min8ub:
      __m64 vec_min8ub (__m64 left, __m64 right)
        Min 8 8-bit unsigned chars

    vec_min4sh:
      __m64 vec_min4sh (__m64 left, __m64 right)
        Min 4 16-bit shorts

    vec_sumabsdiffs8ub:
      __m64 vec_sumabsdiffs8ub (__m64 left, __m64 right)
        Sum absolute differences of 8 8-bit unsigned chars

    vec_Abs8sb:
      __m64 vec_Abs8sb (__m64 v)
        Absolute value 8 8-bit chars

    vec_Abs4sh:
      __m64 vec_Abs4sh (__m64 v)
        Absolute value 4 16-bit shorts

    vec_Abs2sw:
      __m64 vec_Abs2sw (__m64 v)
        Absolute value 2 32-bit ints

    vec_horizontalAdd4sh:
      __m64 vec_horizontalAdd4sh (__m64 left, __m64 right)
        Horizontally add 2+2 adjacent pairs of 16-bit shorts to 4 16-bit shorts - (a0+a1, a2+a3, b0+b1, b2+b3)

    vec_partialhorizontaladd1sw:
      __m64 vec_partialhorizontaladd1sw (__m64 left, __m64 right)
        Horizontally add 1+1 adjacent pairs of 32-bit ints to 2 32-bit ints - (a0+a1, b0+b1)

    vec_horizontalAddsaturating4sh:
      __m64 vec_horizontalAddsaturating4sh (__m64 left, __m64 right)
        Horizontally add 2+2 adjacent pairs of 16-bit shorts to 4 16-bit shorts with saturation - (a0+a1, a2+a3, b0+b1, b2+b3)

    vec_horizontalSub4sh:
      __m64 vec_horizontalSub4sh (__m64 left, __m64 right)
        Horizontally subtract 2+2 adjacent pairs of 16-bit shorts to 4 16-bit shorts - (a0+a1, a2+a3, b0+b1, b2+b3)

    vec_partialhorizontalsub1sw:
      __m64 vec_partialhorizontalsub1sw (__m64 left, __m64 right)
        Horizontally subtract 1+1 adjacent pairs of 32-bit ints to 2 32-bit ints - (a0+a1, b0+b1)

    vec_horizontalSubsaturating4sh:
      __m64 vec_horizontalSubsaturating4sh (__m64 left, __m64 right)
        Horizontally subtract 2+2 adjacent pairs of 16-bit shorts to 4 16-bit shorts with saturation - (a0+a1, a2+a3, b0+b1, b2+b3)

    vec_Multiply8sbthenhorizontalAddsaturating8sh:
      __m64 vec_Multiply8sbthenhorizontalAddsaturating8sh (__m64 left, __m64 right)
        Multiply 8 8-bit signed chars then add adjacent 16-bit products with signed saturation

    vec_Multiply4shExtractUpper:
      __m64 vec_Multiply4shExtractUpper (__m64 left, __m64 right)
        Multiply 4 16-bit shorts, shift right 14, add 1 and shift right 1 to 4 16-bit shorts

    vec_conditionalNegate8sb:
      __m64 vec_conditionalNegate8sb (__m64 left, __m64 right)
        Negate 8 8-bit chars when mask is negative, zero when zero, else copy

    vec_conditionalNegate4sh:
      __m64 vec_conditionalNegate4sh (__m64 left, __m64 right)
        Negate 4 16-bit shorts when mask is negative, zero when zero, else copy

    vec_conditionalNegate2sw:
      __m64 vec_conditionalNegate2sw (__m64 left, __m64 right)
        Negate 2 32-bit ints when mask is negative, zero when zero, else copy

    vec_add4sh:
      __m64 vec_add4sh (__m64 left, __m64 right)
        Add 4 signed 16-bit shorts

    vec_add2sw:
      __m64 vec_add2sw (__m64 left, __m64 right)
        Add 2 32-bit ints

    vec_add8sb:
      __m64 vec_add8sb (__m64 left, __m64 right)
        Add 8 8-bit signed chars

    vec_addsaturating4sh:
      __m64 vec_addsaturating4sh (__m64 left, __m64 right)
        Add 4 signed 16-bit shorts with saturation

    vec_addsaturating8sb:
      __m64 vec_addsaturating8sb (__m64 left, __m64 right)
        Add 8 8-bit signed chars with saturation

    vec_addsaturating4uh:
      __m64 vec_addsaturating4uh (__m64 left, __m64 right)
        Add 4 unsigned 16-bit shorts with saturation

    vec_addsaturating8ub:
      __m64 vec_addsaturating8ub (__m64 left, __m64 right)
        Add 8 8-bit unsigned chars with saturation

    vec_subtract4sh:
      __m64 vec_subtract4sh (__m64 left, __m64 right)
        Subtract 4 signed 16-bit shorts

    vec_subtract2sw:
      __m64 vec_subtract2sw (__m64 left, __m64 right)
        Subtract 2 32-bit ints

    vec_subtract8sb:
      __m64 vec_subtract8sb (__m64 left, __m64 right)
        Subtract 8 8-bit signed chars

    vec_subtractsaturating4sh:
      __m64 vec_subtractsaturating4sh (__m64 left, __m64 right)
        Subtract 4 signed 16-bit shorts with saturation

    vec_subtractsaturating8sb:
      __m64 vec_subtractsaturating8sb (__m64 left, __m64 right)
        Subtract 8 8-bit signed chars with saturation

    vec_subtractsaturating4uh:
      __m64 vec_subtractsaturating4uh (__m64 left, __m64 right)
        Subtract 4 unsigned 16-bit shorts with saturation

    vec_subtractsaturating8ub:
      __m64 vec_subtractsaturating8ub (__m64 left, __m64 right)
        Subtract 8 8-bit unsigned chars with saturation

  Comparison:

    vec_compareeq4sh:
      __m64 vec_compareeq4sh (__m64 left, __m64 right)
        Compare 4 signed 16-bit shorts for == to mask

    vec_compareeq2sw:
      __m64 vec_compareeq2sw (__m64 left, __m64 right)
        Compare 2 32-bit ints for == to mask

    vec_compareeq8sb:
      __m64 vec_compareeq8sb (__m64 left, __m64 right)
        Compare 8 8-bit signed chars for == to mask

    vec_comparegt4sh:
      __m64 vec_comparegt4sh (__m64 left, __m64 right)
        Compare 4 signed 16-bit shorts for > to mask

    vec_comparegt2sw:
      __m64 vec_comparegt2sw (__m64 left, __m64 right)
        Compare 2 32-bit ints for > to mask

    vec_comparegt8sb:
      __m64 vec_comparegt8sb (__m64 left, __m64 right)
        Compare 8 8-bit signed chars for > to mask

  Shift:

    vec_shiftrightlogical1dimmediate:
      __m64 vec_shiftrightlogical1dimmediate (__m64 v, intlit8 count)
        Shift 64-bit long long right logical

    vec_shiftright2dw:
      __m64 vec_shiftright2dw (__m64 left, __m64 right, int count)
        Shift 64+64-bits right into 64-bits

  Permute:

    vec_permute4himmediate:
      __m64 vec_permute4himmediate (__m64 from, intlit8 selector)
        Shuffle 4 16-bit shorts

.
