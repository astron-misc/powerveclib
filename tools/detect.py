#! python
import sys
import fileinput
import re
import string

#function used to identify which Intel SIMD functions need translating to VECLIB
def identify_functions():
    input_file = open(sys.argv[2])
    input_file_lines = input_file.readlines()

    output_list_file = open (sys.argv[3], 'w')
    
    non_IBM_functions = list()

    for line in input_file_lines:
        input_file_words = line.split()
        for word in input_file_words:
            for occurence in re.finditer(r'(_mm\w*_\w+_\w+)|(_m_\w+)', word):
                non_IBM_functions.append(occurence.group())

    non_IBM_functions = list(set(non_IBM_functions))
    non_IBM_functions.sort()

    output_list_file.write("The following functions require mapping to VECLIB:\n\n")
    for function in non_IBM_functions:
        output_list_file.write(function + "\n")
    input_file.close()
    output_list_file.close()

#converts IBM's deprecated VECLIB function names to IBM's new VECLIB function names
def deprecated_to_new():
    
    testfile = open(sys.argv[2])
    finaltest = open (sys.argv[3], 'w')

    testfile_lines = testfile.readlines()

    #first layer, going through each line in file with code
    for testfile_line in testfile_lines:
        testfile_words = re.split(r'(\s+)', testfile_line)
        #second layer, going through each word in the line to check if it requires changing
        for word in testfile_words:
            #checking to see if it is the word (can add more as changes are needed to final result)
            if ('vec_' in word) :
                deprecated_new = open('deprecated_new_functions.csv')
                deprecated_new_lines = deprecated_new.readlines()
                #third layer, scanning through the csv file for matches
                for deprecated_new_line in deprecated_new_lines:
                    spreadsheet_words = deprecated_new_line.split()
                    #if the first word from the csv matches the word we are trying to swap
                    #swap them when outputting to a new file (additional formatting can be applied to output)
                    if (spreadsheet_words[0] == word):
                        word = spreadsheet_words[1]
                    elif (spreadsheet_words[0] in word):
                        word = word.replace(spreadsheet_words[0], spreadsheet_words[1])   
                deprecated_new.close()
            finaltest.write(word)
    finaltest.close()
    return
    

#standard output function when script/tool is called incorrectly
def script_information():
    print ()
    print ("[request type] [filename 1] [filename 2]\n")
    print ("[request type] ranges from 1-2:")
    print ("\t1: Identify functions required for mapping to VECLIB")
    print ("\t2: Map functions from IBM's deprecated VECLIB function names to IBM's new VECLIB function names")
    print ("[filename 1] is for the input file")
    print ("[filename 2] is for the output file (enter desired file name)\n")
    print ("e.g.: <script_name.py> 2 input.c output.c\n")

#checking to see if user entered input correct (# of params) otherwise output information on how to use script/tool
if (len(sys.argv) == 4):
    user_input = int(sys.argv[1])
else:
    script_information()
    sys.exit("\nEnd of script")

#calls the respective function based on the user input
if (user_input == 1):
    identify_functions()
    sys.exit("\nEnd of script")
elif (user_input == 2):
    deprecated_to_new()
    sys.exit("\nEnd of script")
else:
    script_information()
    sys.exit("\nEnd of script")
    
